import React from "react";
import {Text, TouchableOpacity, View} from "react-native";
import moment from 'moment-hijri';
import Modal from 'react-native-modalbox';
import {Ionicons} from "@expo/vector-icons";

class HijriCalendar extends React.Component {
    state = {day: 1, month: 1, year: 1400, valid: true, date: this.props.date, weekDay: null};

    componentWillMount() {
        this.parseDate(this.props.date);
    }

    componentWillReceiveProps(nextProps) {
        this.parseDate(nextProps.date);
    }

    parseDate(date) {
        const dateObject = moment(date, 'iYYYY/iM/iD')
        this.setState({
            date: date,
            day: parseInt(dateObject.format('iD')),
            month: parseInt(dateObject.format('iM')),
            year: parseInt(dateObject.format('iYYYY'))
        });
    }

    validateDate(param) {
        let day = param && ("day" in param) ? param.day : this.state.day;
        let month = param && ("month" in param) ? param.month : this.state.month;
        let year = param && ("year" in param) ? param.year : this.state.year;
        let date = year + '/' + month + '/' + day;

        let dateObject = moment(date, 'iYYYY/iM/iD');
        let isValid = dateObject.isValid();
        if (isValid) {
            this.setState({day, month, year, valid: isValid, weekDay: week[dateObject.format('ddd')]})
        } else {
            this.setState({day, month, year, valid: isValid, weekDay: '-'});
        }
    }

    onSelectDate() {
        let month = this.state.month < 10 ? '0' + this.state.month : this.state.month;
        let day = this.state.day < 10 ? '0' + this.state.day : this.state.day;
        let date = this.state.year + '/' + month + '/' + day;
        this.props.onDateSelected(date);
        this.refs.modal.close();
    }

    render() {
        return (
            <View>
                <TouchableOpacity onPress={() => {
                    this.validateDate();
                    this.refs.modal.open();
                }}>
                    <Text style={this.props.style}>{this.state.date}</Text>
                </TouchableOpacity>
                <Modal ref={"modal"} position={"center"} coverScreen={true} ref={"modal"} position={"center"}
                       coverScreen={true} style={{
                    position: 'absolute',
                    top: 0,
                    left: 0,
                    right: 0,
                    bottom: 0,
                    backgroundColor: 'transparent',
                    justifyContent: 'center',
                    alignItems: 'center'
                }}>
                    <View style={{
                        backgroundColor: '#fff',
                        borderWidth: 2,
                        borderColor: '#ccc',
                        width: 300,
                        height: 360,


                    }}>
                        <View style={{flex: 1}}>

                            <View style={{flex: 1, flexDirection: 'row', height: 50}}>
                                <View style={styles.buttonWrapperStyle}
                                      onPress={() => this.refs.modal.close()}>
                                    <Text style={{fontSize: 20, fontWeight: 'bold'}}>{this.state.weekDay}</Text>
                                </View>
                            </View>

                            <View style={{flex: 3, flexDirection: 'row', height: 70}}>
                                <TouchableOpacity
                                    style={styles.iconWrapperStyle}
                                    onPress={() => {
                                        this.validateDate({year: this.state.year < 1499 ? this.state.year + 1 : 1300});
                                    }}>
                                    <Ionicons name='ios-arrow-up' size={50}/>
                                </TouchableOpacity>
                                <TouchableOpacity
                                    style={styles.iconWrapperStyle}
                                    onPress={() => {
                                        this.validateDate({month: this.state.month === 12 ? 1 : this.state.month + 1});
                                    }}>
                                    <Ionicons name='ios-arrow-up' size={50}/>
                                </TouchableOpacity>
                                <TouchableOpacity
                                    style={styles.iconWrapperStyle}
                                    onPress={() => {
                                        this.validateDate({day: this.state.day === 30 ? 1 : this.state.day + 1});
                                    }}>
                                    <Ionicons name='ios-arrow-up' size={50}/>
                                </TouchableOpacity>
                            </View>

                            <View style={{flex: 3, flexDirection: 'row', height: 100}}>
                                <View style={styles.textWrapperStyle}>
                                    <Text style={styles.textStyle}>{this.state.year}</Text>
                                </View>
                                <View style={styles.textWrapperStyle}>
                                    <Text style={styles.textStyle}>
                                        {this.state.month < 10 ? '0' + this.state.month : this.state.month}
                                    </Text>
                                </View>
                                <View style={styles.textWrapperStyle}>
                                    <Text style={styles.textStyle}>
                                        {this.state.day < 10 ? '0' + this.state.day : this.state.day}
                                    </Text>
                                </View>
                            </View>

                            <View style={{flex: 3, flexDirection: 'row', height: 70}}>
                                <TouchableOpacity
                                    style={styles.iconWrapperStyle}
                                    onPress={() => {
                                        this.validateDate({year: this.state.year > 1300 ? this.state.year - 1 : 1499});
                                    }}>
                                    <Ionicons name='ios-arrow-down' size={50}/>
                                </TouchableOpacity>
                                <TouchableOpacity
                                    style={styles.iconWrapperStyle}
                                    onPress={() => {
                                        this.validateDate({month: this.state.month === 1 ? 12 : this.state.month - 1});
                                    }}>
                                    <Ionicons name='ios-arrow-down' size={50}/>
                                </TouchableOpacity>
                                <TouchableOpacity
                                    style={styles.iconWrapperStyle}
                                    onPress={() => {
                                        this.validateDate({day: this.state.day === 1 ? 30 : this.state.day - 1});
                                    }}>
                                    <Ionicons name='ios-arrow-down' size={50}/>
                                </TouchableOpacity>
                            </View>

                            <View style={{flex: 2, flexDirection: 'row', height: 70}}>
                                <TouchableOpacity style={styles.buttonWrapperStyle}
                                                  onPress={() => {
                                                      this.parseDate(this.props.date);
                                                      this.refs.modal.close();
                                                  }}>
                                    <Ionicons name='md-close' color='red' size={50}/>
                                </TouchableOpacity>
                                <TouchableOpacity style={styles.buttonWrapperStyle} disabled={!this.state.valid}
                                                  onPress={this.onSelectDate.bind(this)}>
                                    <Ionicons name='md-checkmark' color={this.state.valid ? 'green' : 'gray'}
                                              size={50}/>
                                </TouchableOpacity>
                            </View>

                        </View>
                    </View>
                </Modal>
            </View>
        );
    }
}

const week = {
    Sat: 'السبت',
    Sun: 'الأحد',
    Mon: 'الاثنين',
    Tue: 'الثلاثاء',
    Wed: 'الأربعاء',
    Thu: 'الخميس',
    Fri: 'الجمعة',
};

const styles = {
    buttonWrapperStyle: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        height: 50,
        marginRight: 15,
        marginLeft: 15,
        marginTop: -15,
        borderRadius: 8,
        backgroundColor: '#ddd'
    },
    iconWrapperStyle: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        height: 50,
        marginRight: 15,
        marginLeft: 15,
    },
    textWrapperStyle: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        borderWidth: 2,
        borderColor: '#ccc',
        height: 50,
        marginRight: 15,
        marginLeft: 15,
    },
    textStyle: {
        fontSize: 20,
        fontWeight: 'bold',
    }
};

export {HijriCalendar};