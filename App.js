import React from 'react';
import {StyleSheet, View} from 'react-native';
import {HijriCalendar} from "./components/HijriCalendar";

export default class App extends React.Component {
    state = {date: '1439/08/19'};

    render() {
        console.log(this.state.date + 'H');
        return (
            <View style={styles.container}>
                <HijriCalendar
                    style={{ fontSize: 22}}
                    date={this.state.date}
                    onDateSelected={(date) => this.setState({date})}/>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
});
